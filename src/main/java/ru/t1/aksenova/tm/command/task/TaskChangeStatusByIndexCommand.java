package ru.t1.aksenova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-change-status-by-index";

    @NotNull
    public static final String DESCRIPTION = "Change task status by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[CHANGE TASK STATUS BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("ENTER TASK STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();

        @Nullable final Status status = Status.toStatus(statusValue);
        getTaskService().changeTaskStatusByIndex(userId, index, status);
    }

}
