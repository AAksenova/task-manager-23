package ru.t1.aksenova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-update-by-index";

    @NotNull
    public static final String DESCRIPTION = "Update project by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER PROJECT INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("ENTER PROJECT NAME:");
        @Nullable final String name = TerminalUtil.nextLine();

        System.out.println("ENTER PROJECT DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();

        getProjectService().updateByIndex(userId, index, name, description);
    }

}
