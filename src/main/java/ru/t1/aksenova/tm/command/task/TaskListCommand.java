package ru.t1.aksenova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.enumerated.TaskSort;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    public static final String DESCRIPTION = "Show list tasks.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @Nullable final String userId = getUserId();
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(TaskSort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();

        TaskSort sort = TaskSort.toSort(sortType);
        if (sort == null) {
            sort = TaskSort.toSort("BY_CREATED");
            System.out.println("DEFAULT SORT: BY_CREATED");
        }
        @Nullable final List<Task> tasks = getTaskService().findAll(userId, sort.getComparator());
        int index = 1;
        for (@Nullable final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName() + ": " + task.getDescription());
            index++;
        }
    }

}
