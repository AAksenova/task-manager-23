package ru.t1.aksenova.tm.api.service;

import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.model.User;

public interface IUserService extends IService<User> {

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeOne(User model);

    User removeOneByEmail(String email);

    User removeOneByLogin(String login);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String lastName, String middleName);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    void lockUserByLogin(String login);

    void unlockUserByLogin(String login);

}
