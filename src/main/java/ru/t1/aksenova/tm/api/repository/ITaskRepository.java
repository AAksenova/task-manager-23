package ru.t1.aksenova.tm.api.repository;

import ru.t1.aksenova.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    Task create(String userId, String name, String description);

    List<Task> findAllByProjectId(String userId, String projectId);

}
